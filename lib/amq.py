import json
import logging
import stomp
import uuid


log = logging.getLogger(__name__)


class ActiveMqListener(stomp.ConnectionListener):
    """
    ActiveMQ client
    """

    def __init__(self, host, port, cert, key):
        """
        Constructor
        """
        self.id = str(uuid.uuid4())
        self.connection = stomp.Connection([(host, port)])
        self.connection.set_ssl(
            for_hosts=[(host, port)],
            ca_certs=None,
            key_file=key, cert_file=cert
        )
        self.connection.set_listener('MessagingListener', self)
        self.topics = dict()

    def start(self):
        self.connection.start()
        self.connection.connect()

    def add_topic(self, topic, callback):
        self.topics[topic] = callback

    def on_connecting(self, host_and_port):
        log.debug('ActiveMQ connected socket to %s' % str(host_and_port))

    def on_connected(self, headers, body):
        log.info('ActiveMQ connected %s' % headers)
        for topic in self.topics.keys():
            log.info('Subscribe to %s' % topic)
            self.connection.subscribe(
                destination=topic,
                id='activemq-listener-%s' % self.id,
                ack='auto',
                conf={'selector': 'endpnt = "fts3.cern.ch"'}
            )

    def on_disconnected(self):
        log.warning('ActiveMQ connection lost. Try to reconnect...')
        self.connection.connect()

    def on_heartbeat_timeout(self):
        log.warning('ActiveMQ heartbeat timeout. Try to reconnect...')
        self.connection.stop()
        self.connection.start()
        self.connection.connect()

    def on_message(self, headers, message):
        message = message[:-1]  # Skip EOT
        try:
            content = json.loads(message)
            callback = self.topics[headers['destination']]
            callback(content)
        except Exception, e:
            log.warning('Failed to process message: (%s) %s' % (type(e).__name__, e.message))
            log.warning(json.dumps(message))
            raise

    def on_error(self, headers, body):
        log.error('ActiveMQ error: %s' % body)
