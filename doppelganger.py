#!/usr/bin/env python
#
# Subscribe to the state change topic, submit to a different FTS3 the same
# job but with mock:// instead
# Can't really get the job, only the transfers. For the moment being, live with it
#
import errno
import logging
import optparse
import os
import random
import socket
import sys
import threading
import time
import urllib
import urlparse

import fts3.rest.client.easy as fts3

from lib.amq import ActiveMqListener

log = logging.getLogger(__name__)

ERRNO_MAPPING = dict()
for errcode in errno.errorcode:
    category = os.strerror(errcode).replace(' ', '_').upper()
    ERRNO_MAPPING[category] = errcode
ERRNO_MAPPING['GENERAL ERROR'] = errno.EINVAL


def _resolve_brokers(alias, port):
    """
    To consume all the messages, we need to subscribe to all the hosts behind
    the DNS alias.
    :param alias: The DNS alias
    :param port: The TCP port
    :return: A list of tuples (resolved ip, port)
    """
    brokers = list()
    for family, _, _, _, addr in socket.getaddrinfo(alias, port, 0, 0, socket.IPPROTO_TCP):
        if family == socket.AF_INET:
            ip, port = addr
        elif family == socket.AF_INET6:
            ip, port, _, _ = addr
        else:
            raise Exception('Unsupported family: %s' % family)
        brokers.append((ip, port))
    return brokers


def _rewrite_url(url, checksum, filesize, is_dest, errcode, transfer_errcode, throughput=None):
    """
    Rewrite to a mock url
    """
    parsed = urlparse.urlparse(url)
    query = urlparse.parse_qs(parsed.query)

    if parsed.scheme == 'srm' and 'SFN' in query:
        path = query['SFN'][0]
        del query['SFN']
    else:
        path = parsed.path

    if checksum:
        query['checksum'] = checksum

    if not filesize:
        filesize = 1024
    if is_dest:
        query['size_post'] = filesize
    else:
        query['size'] = filesize

    # Emulate duration with the stored throughput, or random otherwise
    if not throughput:
        throughput = random.randint(1024 * 1024, 50 * 1024 * 1024)
    duration = int(filesize / throughput)
    query['time'] = max(min(duration, 600), 10)

    if errcode:
        query['errno'] = errcode
    if transfer_errcode:
        query['transfer_errno'] = transfer_errcode

    return urlparse.urlunparse(('mock', parsed.hostname, path, None, urllib.urlencode(query), parsed.fragment))


def _probabilistic_error(stats):
    """
    From a stat dictionary, throw a coin and decide if an error
    takes place following the distribution seen in the 'real-life'
    Return source_errno, destination_errno, transfer_errno
    """
    if stats is None:
        return None, None, None

    total = stats.get('total', 0)
    if not total:
        return None, None, None

    dice = random.random() * total
    accumulate = 0
    for key, value in stats.iteritems():
        if not key.startswith('error') and key != 'ok':
            continue
        start = accumulate
        accumulate += value
        if start <= dice < accumulate:
            if key == 'ok':
                return None, None, None

            _, phase, errcode = key.split('||')
            if phase == 'SOURCE':
                return int(errcode), None, None
            elif phase == 'TRANSFER':
                return None, None, int(errcode)
            else:
                return None, int(errcode), None

    log.error("%d %.2f %d" % (total, dice, accumulate))
    raise AssertionError('Should not be reached')


def _get_se(url):
    """
    Extract the storage from the url
    """
    parsed = urlparse.urlparse(url)
    return '%s://%s' % (parsed.scheme, parsed.hostname)


def _map_to_errno(category):
    """
    Return the error code corresponding to the error category
    """
    if category in ERRNO_MAPPING:
        return ERRNO_MAPPING[category]
    else:
        log.warning('Unknown category: %s' % category)
        return errno.ECOMM


class Worker(threading.Thread):
    """
    Worker thread
    """

    def __init__(self, endpoint, vos, fts, broker, cert, key):
        """
        Constructor
        """
        super(Worker, self).__init__()
        self.daemon = True

        self.fts3_context = fts3.Context(endpoint=endpoint)
        self.amq = ActiveMqListener(
            host=broker[0], port=broker[1],
            cert=cert, key=key,
        )
        self.amq.add_topic('/topic/transfer.fts_monitoring_state', self.process_transfer)
        self.amq.add_topic('/topic/transfer.fts_monitoring_complete', self.process_complete)

        self.channel_stats = dict()
        self.vos = vos
        self.fts = fts

    def run(self):
        log.info("Starting worker")
        self.amq.start()
        while True:
            time.sleep(5)

    def _generate_extra_sources(self, source, checksum, filesize):
        """
        Return a list of alternative sources picking some random known storages
        """
        known_triplets = self.channel_stats.keys()
        if len(known_triplets) == 0:
            return list()

        parsed = urlparse.urlparse(source)
        alternatives = list()
        for i in range(random.randint(1, 5)):
            key = random.choice(known_triplets)
            source_se = urlparse.urlparse(key[1]).hostname

            stats = self.channel_stats.get(key, None)
            source_err, _, _ = _probabilistic_error(stats)

            source_with_new_host = urlparse.urlunparse(
                ('mock', source_se, parsed.path, None, None, None)
            )

            new_source_url = _rewrite_url(
                source_with_new_host, checksum, filesize, False,
                None, None, stats.get('throughput', None)
            )

            alternatives.append(new_source_url)

        return alternatives

    def process_transfer(self, message):
        """
        Submit a transfer with the intercepted urls (except using mock)
        """
        state = message['file_state']
        if state != 'SUBMITTED':
            return
        vo = message['vo_name']
        source = message['src_url']
        dest = message['dst_url']
        metadata = message['file_metadata']
        job_metadata = message['job_metadata']
        endpnt = message['endpnt']

        # Filter vos
        if self.vos and vo not in self.vos:
            return

        # Filter FTS
        if self.fts and endpnt not in self.fts:
            return

        # Make sure we do not retro-feed ourselves!
        if source.startswith('mock'):
            return

        # Only makes sense for Atlas submissions really
        if isinstance(metadata, dict):
            checksum = metadata.get('adler32', None)
            filesize = metadata.get('filesize', None)
            activity = metadata.get('activity', None)
        else:
            checksum = None
            filesize = None
            activity = None

        if isinstance(job_metadata, dict):
            is_multisource = job_metadata.get('multi_sources', False)
        else:
            is_multisource = False

        # Use stored statistics to generate urls that will fail
        source_se = _get_se(source)
        dest_se = _get_se(dest)
        key = (vo, source_se, dest_se)
        stats = self.channel_stats.get(key, None)
        source_err, dest_err, transfer_err = _probabilistic_error(stats)
        if stats:
            throughput = stats.get('throughput', None)
        else:
            throughput = None

        # Rewrite urls
        source = _rewrite_url(source, checksum, filesize, False, source_err, None)
        dest = _rewrite_url(dest, checksum, filesize, True, dest_err, transfer_err, throughput)
        transfer = fts3.new_transfer(
            source, dest, checksum=checksum, filesize=filesize, metadata=metadata, activity=activity
        )

        # If it is a multisource, generate a random number of alternatives
        # and push them
        if is_multisource:
            transfer['sources'].extend(self._generate_extra_sources(source, checksum, filesize))
            transfer['selection_strategy'] = 'auto'

        job = fts3.new_job(transfers=[transfer], verify_checksum=(checksum is not None), overwrite=False)
        try:
            job_id = fts3.submit(self.fts3_context, job)
        except Exception, e:
            log.error('Failed to submit job: %s' % str(e))
            return

        if is_multisource:
            log.info('%s (multisource) %s => %s' % (job_id, source, dest))
        else:
            log.info('%s %s => %s' % (job_id, source, dest))

    def process_complete(self, message):
        """
        Use completion messages to calculate error probabilities, error codes,...
        and emulate as much as possible the production environment
        """
        vo = message['vo']
        source_se = _get_se(message['src_url'])
        dest_se = _get_se(message['dst_url'])
        state = message['t_final_transfer_state']

        key = (vo, source_se, dest_se)
        stats = self.channel_stats.get(key, dict())

        if state == 'Ok':
            stats['ok'] = stats.get('ok', 0) + 1
            start = float(message['tr_timestamp_start'])
            end = float(message['tr_timestamp_complete'])
            size = int(message['f_size'])
            # The timestamps are in milliseconds!
            duration = (end - start) / 1000
            if duration == 0:
                duration = 0.1
            stats['throughput'] = size / duration
        else:
            errno = _map_to_errno(message['tr_error_category'])
            phase = message['t_failure_phase']
            err_key = 'error||%s||%s' % (phase, errno)
            stats[err_key] = stats.get(err_key, 0) + 1
        stats['total'] = stats.get('total', 0) + 1

        self.channel_stats[key] = stats


class Doppelganger(object):
    """
    Mirrors requests
    """

    def __init__(self, opts, *vos):
        """
        Constructor
        """
        self.opts = opts
        self.vos = vos

    def run(self):
        """
        Entry point
        """
        brokers = _resolve_brokers(*self.opts.broker.split(':', 2))
        workers = []
        for broker in brokers:
            worker = Worker(self.opts.endpoint, self.vos, self.opts.fts, broker, self.opts.cert, self.opts.key)
            worker.start()
            workers.append(worker)

        log.info('Waiting for children')
        while True:
            time.sleep(5)


def _setup_logging():
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(logging.DEBUG)
    root_logger.addHandler(handler)
    logging.getLogger('stomp.py').setLevel(logging.WARNING)


if __name__ == '__main__':
    _setup_logging()

    import config
    default_endpoint = "https://%s:%d" % (config.FTS3Host, config.FTS3Port)
    default_broker = "%s:%d" % (config.MsgBrokerHost, config.MsgBrokerPort)

    parser = optparse.OptionParser()
    parser.add_option('--endpoint', default=default_endpoint, help='FTS endpoint')
    parser.add_option('--broker', default=default_broker, help='Broker')
    parser.add_option('--cert', default=config.PublicCert, help='Public certificate')
    parser.add_option('--key', default=config.PrivateKey, help='Private key')
    parser.add_option('--fts', type=str, action='append', help='Mimic only a set of FTS3 hosts')

    opt, args = parser.parse_args()

    doppelganger = Doppelganger(opt, *args)
    doppelganger.run()
