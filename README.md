Doppelganger
============
Little tools that subscribes to `/topic/transfer.fts_monitoring_state` and
submits to the target FTS3 instance (development cluster normally) every transfer
it gets from the broker as `SUBMITTED`, rewriting the SURL as to use mock mock instead.

It is also capable of using filesize and checksum from the file metadata is present
(as it is the case for ATLAS).

Bully
=====
Just submits continuosly transfers for a limited number of pairs, so we fill their queue.

