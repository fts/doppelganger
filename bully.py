#!/usr/bin/env python

import optparse
import logging
import os
import random
import threading
import time
import string
import sys
import urllib
import urlparse

import fts3.rest.client.easy as fts3


# Setup logging
root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stderr)
handler.setLevel(logging.DEBUG)
root_logger.addHandler(handler)

log = logging.getLogger(__name__)


def random_string(length):
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(length))


def random_path():
    return '/'.join(random_string(4) for _ in range(3))


# Worker
class Bully(threading.Thread):
    """
    Assaults FTS3 with continuous submissions
    """

    def __init__(self, opt):
        """
        Constructor
        """
        super(Bully, self).__init__()
        self.fts3_context = fts3.Context(
            endpoint=opt.endpoint,
        )
        self.sources = [opt.source]
        self.destinations = [opt.destination]
        self.sleep = opt.sleep
        self.running = True
        self.njobs = opt.jobs
        self.max = opt.transfer_time_max
        self.min = opt.transfer_time_min
        self.reuse = opt.reuse
        self.replicas = opt.replicas
        if self.replicas and self.reuse:
            raise Exception("Can't combine session reuse and multiple replicas")
        if self.replicas:
            self.nfiles = 1
            self.nreplicas = opt.job_size
        else:
            self.nfiles = opt.job_size
            self.nreplicas = 1
        self.start()

    def _generate_pairs(self):
        """
        Generate a suitable pair for transferring
        """
        source_se = random.choice(self.sources)
        dest_se = random.choice(self.destinations)
        path = random_path()
        file_size = random.randint(1024, 1024*1024*10)

        source_url = []
        for i in range(self.nreplicas):
            source_url.append(urlparse.urlunparse((
                'mock', "%s%d" % (source_se, i), path, None,
                urllib.urlencode(dict(
                    size=file_size
                )),
                None
            )))

        dest_url = urlparse.urlunparse(
            (
                'mock', dest_se, path, None,
                urllib.urlencode(dict(
                    time=random.randint(self.min, self.max),
                    size_pre=0,
                    size_post=file_size
                )),
                None
            )
        )

        return source_url, [dest_url]

    def stop(self):
        """
        Request the thread to stop
        """
        self.running = False

    def run(self):
        """
        Bully the FTS3 server
        """
        while self.njobs > 0 and self.running:
            transfers = []
            for i in range(self.nfiles):
                source, destination = self._generate_pairs()
                transfers.append(dict(
                    sources=source,
                    destinations=destination
                ))
            try:
                job = fts3.new_job(
                    transfers=transfers,
                    verify_checksum=False, overwrite=True,
                    reuse=self.reuse
                )
                job_id = fts3.submit(self.fts3_context, job)
                log.info('%s' % (job_id))
            except Exception, e:
                log.error(str(e))
            self.njobs -= 1
            time.sleep(self.sleep/1000.0)
        log.info("Done")
        self.running = False


if __name__ == '__main__':
    opt = optparse.OptionParser()
    opt.add_option(
        '-s', '--endpoint', type=str,
        default='https://fts3-devel.cern.ch:8446', help='FTS3 endpoint'
    )
    opt.add_option(
        '--source', type=str, default='source.host',
        help='Source storage'
    )
    opt.add_option(
        '--destination', type=str, default='dest.host',
        help='Source storage'
    )
    opt.add_option(
        '--sleep', type=int, default=10,
        help='Milliseconds to sleep between submissions'
    )
    opt.add_option(
        '--job-size', type=int, default=1,
        help='Job size',
    )
    opt.add_option(
        '--jobs', type=int, default=99999,
        help='Number of jobs'
    )
    opt.add_option(
        '--transfer-time-max', type=int, default=300,
        help='Max. time of transfer'
    )
    opt.add_option(
        '--transfer-time-min', type=int, default=30,
        help='Min. time of transfer'
    )
    opt.add_option(
        '--reuse', action='store_true',
        help='Send session reuse jobs'
    )
    opt.add_option(
        '--replicas', action='store_true',
        help='Send multireplica jobs'
    )

    opt, args = opt.parse_args()

    bully = Bully(opt)
    try:
        bully.join()
    except KeyboardInterrupt:
        log.warning('Stopping')
        bully.stop()
        bully.join()
