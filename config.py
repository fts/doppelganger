import os


MsgBrokerHost = 'dashb-mb.cern.ch'
MsgBrokerPort = 61123

PublicCert = os.environ.get('X509_USER_CERT', '/etc/grid-security/hostcert.pem')
PrivateKey = os.environ.get('X509_USER_KEY', '/etc/grid-security/hostkey.pem')

FTS3Host = os.environ.get('FTS3_HOST', 'fts3-devel.cern.ch')
FTS3Port = 8446
