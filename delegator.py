#!/usr/bin/env python
#
# Trigger a delegation periodically, to keep the proxy in FTS3 fresh
#

import logging
import threading
import time
import sys

import fts3.rest.client.easy as fts3


# Setup logging
root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stderr)
handler.setLevel(logging.DEBUG)
root_logger.addHandler(handler)

log = logging.getLogger(__name__)


# Worker
class Delegator(threading.Thread):
    """
    Delegates to FTS3
    """

    def __init__(self, config):
        """
        Constructor
        """
        super(Delegator, self).__init__()
        self.fts3_context = fts3.Context(
            endpoint='https://%s:%d' % (config.FTS3Host, config.FTS3Port)
        )
        self.running = True
        self.start()

    def stop(self):
        """
        Request the thread to stop
        """
        self.running = False

    def run(self):
        """
        Keep delegating
        """
        counter = 0
        while self.running:
            if counter % 60 == 0:
                dlg_id = fts3.delegate(self.fts3_context, force=True)
                log.info('Delegated with id %s' % dlg_id)
                counter = 0
            counter += 1
            time.sleep(1)


if __name__ == '__main__':
    import config

    delegator = Delegator(config)
    try:
        while True:
            delegator.join(5)
    except KeyboardInterrupt:
        log.warning('Stopping')
        delegator.stop()
        delegator.join()
